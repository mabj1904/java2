package se.miun.mabj1904.dt187g.jpaint;

import java.awt.Graphics;
import java.util.Arrays;

/** 
* Represents a circle. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public class Circle extends Shape{

    public static final double PI = 3.14;

    public Circle(Point p, String color) {
        super(p, color);
    }

    public Circle(double x, double y, String color) {
        super(x, y, color);
    }

    public double getRadius(){
        if (points.size() < 2
            || Arrays.asList(points).contains(null)){
            return -1;
        }

        var x1 = points.get(0).getX();
        var x2 = points.get(1).getX();
        var deltaWidth = Math.max(x1, x2) - Math.min(x1, x2);

        var y1 = points.get(0).getY();
        var y2 = points.get(1).getY();
        var deltaHeight = Math.max(y1, y2) - Math.min(y1, y2);

        return Math.sqrt(Math.pow(deltaWidth, 2) + Math.pow(deltaHeight, 2));
    }

    @Override
    public void draw() {
        System.out.println(this.toString());
    }

    @Override
    public void draw(Graphics g) {
        //TODO
    }

    @Override
    public double getCircumference() {
        var radius = getRadius();
        if (radius < 0){
            return -1;
        }

        return PI * 2 * radius;
    }

    @Override
    public double getArea() {
        var radius = getRadius();
        if (radius < 0){
            return -1;
        }

        return PI * Math.pow(radius, 2);
    }
    
    @Override
    public String toString(){
        var radius = getRadius();
        var result = String.format("Circle { Start: %s, End: %s, Radius: %s, Color: %s }", 
            (points != null && points.size() > 1 && points.get(0) != null) ? points.get(0).toString() : "N/A",
            hasEndpoint() ? points.get(1).toString() : "N/A",
            radius > -1 ? radius : "N/A",
            (color != null || !color.isEmpty()) ? color : "N/A");
        
        return result;
    }

    @Override
    public boolean hasEndpoint() {
        return points != null && points.size() > 1 && points.get(1) != null;
    }
}
