package se.miun.mabj1904.dt187g.jpaint;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.plaf.DimensionUIResource;

public class JPaintFrame extends JFrame {
    public JPaintFrame() {
        super();
        initFrame();
    }

    public JPaintFrame(String title) {
        super(title);
        initFrame();
    }

    private void initFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.white);
        setLayout(new BorderLayout());
        setIconImage(new ImageIcon("assets/logo.jpg").getImage());
        setSize(400, 300);

        String title = getTitle();
        if (title == null || title.isEmpty())
            setTitle("PeePooPaint");

        initComponents();

        setVisible(true);
    }

    private void initComponents(){
        //Top row
        JPanel topRow = new JPanel(new BorderLayout());

        //Top row - Menubar
        JMenuBar menuBar = new JMenuBar();

        JMenu menuFile = new JMenu("File");
        JMenuItem itemNew = new JMenuItem("New...");
        JMenuItem itemSaveAs = new JMenuItem("Save as...");
        JMenuItem itemLoad = new JMenuItem("Load...");
        JMenuItem itemExit = new JMenuItem("Exit");

        menuFile.add(itemNew);
        menuFile.add(itemSaveAs);
        menuFile.add(itemLoad);
        menuFile.add(new JSeparator());
        menuFile.add(itemExit);
        menuBar.add(menuFile);

        JMenu menuEdit = new JMenu("Edit");
        JMenuItem itemUndo = new JMenuItem("Undo");
        JMenuItem itemName = new JMenuItem("Name...");
        JMenuItem itemAuthor = new JMenuItem("Author...");

        menuEdit.add(itemUndo);
        menuEdit.add(itemName);
        menuEdit.add(itemAuthor);
        menuBar.add(menuEdit);

        topRow.add(menuBar, BorderLayout.PAGE_START);

        //Top row - Toolbar
        JPanel toolBar = new JPanel(new BorderLayout());
        JPanel colorsContainer = new JPanel(new GridLayout(1, 5));

        JPanel colorPickerGreen = new JPanel();
        colorPickerGreen.setBackground(Color.green);
        JPanel colorPickerBlue = new JPanel();
        colorPickerBlue.setBackground(Color.blue);
        JPanel colorPickerBlack = new JPanel();
        colorPickerBlack.setBackground(Color.black);
        JPanel colorPickerRed = new JPanel();
        colorPickerRed.setBackground(Color.red);
        JPanel colorPickerYellow = new JPanel();
        colorPickerYellow.setBackground(Color.yellow);

        var shapePicker = new JComboBox<String>(new String[] { "Rectangle", "Circle" });
        shapePicker.setMaximumSize(new DimensionUIResource(60, 40));

        colorsContainer.add(colorPickerGreen);
        colorsContainer.add(colorPickerBlue);
        colorsContainer.add(colorPickerBlack);
        colorsContainer.add(colorPickerRed);
        colorsContainer.add(colorPickerYellow);

        toolBar.add(colorsContainer, BorderLayout.CENTER);
        toolBar.add(shapePicker, BorderLayout.LINE_END);
        topRow.add(toolBar, BorderLayout.CENTER);

        //Painting area
        JPanel paintingPanel = new JPanel();
        paintingPanel.setBackground(Color.white);

        //Footer
        JPanel footer = new JPanel(new BorderLayout());

        //TODO: Show current coordinates
        JLabel currentCoordinates = new JLabel(String.format("Coordinates: %s, %s", 0, 0));

        //TODO: Show selected color
        JPanel selectedColorContainer = new JPanel(new FlowLayout());
        JLabel pickedColorText = new JLabel("Selected color: ");
        JPanel pickedColor = new JPanel();
        pickedColor.setBackground(Color.green);
        selectedColorContainer.add(pickedColorText);
        selectedColorContainer.add(pickedColor);
        
        footer.add(currentCoordinates, BorderLayout.LINE_START);
        footer.add(selectedColorContainer, BorderLayout.LINE_END);

        //Add components to frame
        add(topRow, BorderLayout.PAGE_START);
        add(paintingPanel, BorderLayout.CENTER);
        add(footer, BorderLayout.PAGE_END);
    }
}
