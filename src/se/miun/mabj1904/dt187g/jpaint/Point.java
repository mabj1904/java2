package se.miun.mabj1904.dt187g.jpaint;

/** 
* Represents a single point in a coordinate system. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX(){
        return x;
    }
    public void setX(double x){
        this.x = x;
    }
    
    public double getY(){
        return y;
    }
    public void setY(double y){
        this.y = y;
    }

    @Override
    public String toString(){
        return String.format("%s, %s", x, y);
    }
}
