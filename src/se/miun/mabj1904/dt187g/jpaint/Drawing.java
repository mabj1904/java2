package se.miun.mabj1904.dt187g.jpaint;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

/** 
* Represents a drawing consisting of shapes and metadata. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public class Drawing implements Drawable {
    String name;
    String author;
    Collection<Shape> shapes;

    public Drawing() {
        super();

        shapes = new ArrayList<Shape>();
    }

    public Drawing(String name, String author) {
        super();

        shapes = new ArrayList<Shape>();
        this.author = author;
        this.name = name;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getAuthor(){
        return author;
    }
    public void setAuthor(String author){
        this.author = author;
    }

    public void addShape(Shape shape){
        if (shape != null){
            shapes.add(shape);
        }
    }

    public int getSize(){
        return shapes.size();
    }

    public double getTotalCircumference(){
        double result = 0;
        for (Shape shape : shapes) {
            var circ = shape.getCircumference();
            if (circ > -1){
                result += circ;
            }
        }

        return result;
    }

    public double getTotalArea(){
        double result = 0;
        for (Shape shape : shapes) {
            var area = shape.getArea();
            if (area > -1){
                result += area;
            }
        }

        return result;
    }

    /**
     * Method that saves this Drawing to a file of the name provided.
     * @param fileName name to give saved Drawing file.
     * @return true if saved, othervise false
     * @throws DrawingException if any vital Drawing information is missing.
     */
    public boolean save(String fileName) throws DrawingException {
        boolean nameIsEmpty = name == null || name.isEmpty();
        boolean authorIsEmpty = author == null || author.isEmpty();
        if (nameIsEmpty || authorIsEmpty){
            String msg = "The drawing is missing %s";
            if (nameIsEmpty && authorIsEmpty)
                msg = String.format(msg, "author and name");
            else if (nameIsEmpty)
                msg = String.format(msg, "name");
            else if (authorIsEmpty)
                msg = String.format(msg, "author");

            throw new DrawingException(msg);
        }

        return true;
    }

    @Override
    public void draw() {
        var shapeStrings = shapes.stream()
            .map(o -> o.toString())
            .toList();
        var shapesString = String.join(",\n", shapeStrings);

        System.out.println(String.format("A drawing by %s called %s\n%s", 
            author == null ? "" : author, 
            name == null ? "" : name, 
            shapesString));
    }

    @Override
    public void draw(Graphics g) {
        //TODO
    }

    @Override
    public String toString(){
        var circumference = getTotalCircumference();
        var area = getTotalArea();
        return String.format("Drawing { Name: %s, Author: %s, Size: %s, Circumference: %s, Area %s }",
        name == null ? "" : name,
            author == null ? "" : author,
            getSize(),
            circumference,
            area);
    }
}
