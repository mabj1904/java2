package se.miun.mabj1904.dt187g.jpaint;

import java.util.ArrayList;

/** 
* Base superclass for all geometric shapes. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public abstract class Shape implements Drawable {
    protected String color;
    protected ArrayList<Point> points;

    public Shape(double x, double y, String color) {
        this(new Point(x, y), color);
    }

    public Shape(Point p, String color) {
        this.points = new ArrayList<Point>(2);
        this.points.add(p);

        this.color = color;
    }

    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }

    public abstract double getCircumference();
    public abstract double getArea();
    public abstract boolean hasEndpoint();

    public void addPoint(Point p){
        if (points.size() > 1){
            points.set(1, p);
        }
        else {
            points.add(1, p);
        }
    }
    public void addPoint(double x, double y){
        addPoint(new Point(x, y));
    }
}
