package se.miun.mabj1904.dt187g.jpaint;

import java.awt.Graphics;
import java.util.Arrays;

/** 
* Represents a rectangle. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public class Rectangle extends Shape {

    public Rectangle(Point p, String color) {
        super(p, color);
    }

    public Rectangle(double x, double y, String color) {
        super(x, y, color);
    }

    public double getWidth(){
        if (!canCalculateMeasurements()){
            return -1;
        }
        
        var x1 = points.get(0).getX();
        var x2 = points.get(1).getX();
        return Math.max(x1, x2) - Math.min(x1, x2);
    }

    public double getHeight(){
        if (!canCalculateMeasurements()){
            return -1;
        }

        var y1 = points.get(0).getY();
        var y2 = points.get(1).getY();
        return Math.max(y1, y2) - Math.min(y1, y2);
    }

    private boolean canCalculateMeasurements(){
        return points.size() > 1
            && !Arrays.asList(points).contains(null);
    }

    @Override
    public void draw() {
        System.out.println(this.toString());
    }

    @Override
    public void draw(Graphics g) {
        //TODO
    }

    @Override
    public double getCircumference() {
        var width = getWidth();
        var height = getHeight();
        if (width < 0 || height < 0){
            return -1;
        }

        return (2 * width) + (2 * height);
    }

    @Override
    public double getArea() {
        var width = getWidth();
        var height = getHeight();
        if (width < 0 || height < 0){
            return -1;
        }

        return width * height;
    }
    
    @Override
    public String toString(){
        var width = getWidth();
        var height = getHeight();
        var result = String.format("Rectangle { Start: %s, End: %s, Width: %s, Height: %s, Color: %s }", 
            (points != null && points.size() > 1 && points.get(0) != null) ? points.get(0).toString() : "N/A",
            hasEndpoint() ? points.get(1).toString() : "N/A",
            width > -1 ? width : "N/A",
            height > -1 ? height : "N/A",
            (color != null || !color.isEmpty()) ? color : "N/A");
        
        return result;
    }

    @Override
    public boolean hasEndpoint() {
        return points != null && points.size() > 1 && points.get(1) != null;
    }
}
