package se.miun.mabj1904.dt187g.jpaint;

import java.awt.*;

/** 
* Base interface for a drawable class. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public interface Drawable {
    void draw();
    void draw(Graphics g);
}
