package se.miun.mabj1904.dt187g.jpaint;

/** 
* Exception thrown by Drawing. 
* 
* @author Marcus Björkman (mabj1904) 
* @version 1.0  
*/
public class DrawingException extends Exception{
    public DrawingException() {
        super();
    }

    public DrawingException(String msg) {
        super(msg);
    }
}
